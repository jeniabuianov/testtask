require('./bootstrap');
window.Vue = require('vue');
import Vue from 'vue';

Vue.component('countries-list', require('./components/CountriesList').default);

const app = new Vue({
    el: '#app',
});
