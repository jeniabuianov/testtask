<?php

namespace App\Console\Commands;

use App\Components\Convertor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class Convert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:countries {--input-file=} {--output-file=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->hasOption('input-file') || !$this->hasOption('output-file'))
            dd("Please introduce input-file option and output-file option");
        $inputFile = htmlspecialchars($this->option('input-file'),3);
        $outputFile = htmlspecialchars($this->option('output-file'),3);

        $inputExtension = explode('.',$inputFile);
        $inputExtension = end($inputExtension);

        $outputExtension = explode('.',$outputFile);
        $outputExtension = end($outputExtension);

        if (!Storage::exists($inputFile)){
            dd("File ".$inputFile." doesn't exists");
        }

        $convertor = new Convertor(null);
        $inputFormatConvertor = $convertor->detectFormatConvertor($inputExtension);
        $inputFormatConvertor = new $inputFormatConvertor($inputFile);
        $outputFormatConvertor = $convertor->detectFormatConvertor($outputExtension);
        $outputFormatConvertor = new $outputFormatConvertor(null);

        $file = $outputFormatConvertor->fromCollectionToFormat($inputFormatConvertor->getCollection()->toArray());
        Storage::put($outputFile,$file);

        echo "File was converted with success\n";

        return 0;
    }
}
