<?php


namespace App\Components;


use Illuminate\Support\Facades\Storage;

class XMLConvertor extends Convertor {

    public function readFile()
    {
        $content = Storage::get($this->file);
        $xml = simplexml_load_string($content,"SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $this->array = json_decode($json,TRUE)['element'];
    }

    public function fromCollectionToFormat($array){
        $xml = new \SimpleXMLElement('<root/>');
        foreach ($array as $i=>$item){
            $element = $xml->addChild('element');
            $element->addChild('country',$item['country']);
            $element->addChild('capital',$item['capital']);
        }

        return $xml->asXML();
    }
}
