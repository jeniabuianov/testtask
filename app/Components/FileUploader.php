<?php
namespace App\Components;

use Illuminate\Http\Request;

class FileUploader{
    private Request $request;
    private $list;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->storeFile();
        $this->convertToCollection();
    }

    private function storeFile(){
        $this->request->file('uploading')->storeAs('',$this->request->file('uploading')->getClientOriginalName());
    }

    private function convertToCollection(){
        $convertor = null;
        if ($this->request->file('uploading')->getClientOriginalExtension()=="csv")
            $convertor = new CSVConvertor($this->request->file('uploading')->getClientOriginalName());

        if ($this->request->file('uploading')->getClientOriginalExtension()=="json")
            $convertor = new JSONConvertor($this->request->file('uploading')->getClientOriginalName());

        if ($this->request->file('uploading')->getClientOriginalExtension()=="xml")
            $convertor = new XMLConvertor($this->request->file('uploading')->getClientOriginalName());

        $this->list = $convertor->getCollection();
    }

    public function getList(){
        return $this->list;
    }
}
