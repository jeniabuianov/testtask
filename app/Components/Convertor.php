<?php


namespace App\Components;


class Convertor implements IConvertor
{
    public $file;
    private $collection;
    public $array;

    public function __construct($file)
    {
        if (!is_null($file) && !empty($file)) {
            $this->file = $file;
            $this->readFile();
            $this->convertToCollection();
        }
    }

    public function convertToCollection()
    {
        $this->collection = collect($this->array);
    }

    public function detectFormatConvertor($format){
        $format = mb_strtolower(htmlspecialchars($format,3));
        if(!in_array($format,['csv','xml','json']))
            return null;

        if ($format=="csv"){
            return CSVConvertor::class;
        }
        if ($format=="xml"){
            return XMLConvertor::class ;
        }
        if ($format=="json"){
            return JSONConvertor::class;
        }
    }

    public function convertCollectionToFormat($format){
        $formatConvertor = $this->detectFormatConvertor($format);
        if (!is_null($formatConvertor))
            return new $formatConvertor($this->file);
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function fromCollectionToFormat($array){}
    public function readFile(){}
}
