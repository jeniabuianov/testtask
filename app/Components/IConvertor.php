<?php


namespace App\Components;

interface IConvertor
{
    public function convertToCollection();
    public function readFile();
    public function getCollection();
    public function fromCollectionToFormat($array);
}
