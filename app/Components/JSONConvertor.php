<?php


namespace App\Components;


use Illuminate\Support\Facades\Storage;

class JSONConvertor extends Convertor {

    public function readFile()
    {
        $content = Storage::get($this->file);
        $this->array = json_decode($content,true);
    }

    public function fromCollectionToFormat($array){

        return json_encode($array);
    }
}
