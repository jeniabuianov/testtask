<?php


namespace App\Components;


use Illuminate\Support\Facades\Storage;

class CSVConvertor extends Convertor {

    public function readFile()
    {
        $content = Storage::get($this->file);
        $lines = explode("\n",$content);
        for ($i=1;$i<count($lines);$i++){
            if (empty($lines[$i])) continue;
            $row = explode(',',$lines[$i]);
            $this->array[] = [
                'country' => $row[0],
                'capital' => $row[1]
            ];
        }
    }

    public function fromCollectionToFormat($array){
        $result = ["Country,Capital"];
        foreach ($array as $i=>$item){
            $result[] = $item['country'].",".$item['capital'];
        }

        return implode("\n",$result);
    }


}
