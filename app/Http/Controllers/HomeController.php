<?php

namespace App\Http\Controllers;

use App\Components\Convertor;
use App\Components\FileUploader;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('home')->render();
    }

    public function upload(Request $request){
        $fileUploader = new FileUploader($request);

        return response()->json([
            'items' => $fileUploader->getList()
        ]);
    }

    public function download(Request $request){
        $convertor = new Convertor(null);
        $formatConvertor = $convertor->convertCollectionToFormat($request->post('format'));

        return $formatConvertor->fromCollectionToFormat($request->post('items'));
    }
}
